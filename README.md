# Tarea 2: Tarea de temporización 
## Universidad de Costa Rica
### Escuela de Ingeniería Eléctrica
#### IE-0411: Microelectrónica
##### Emmanuel Rivel Montero. B65868

## Última revisión: 
Último commit realizado el: 09/10/20

## Abstract
En la presente, se evalúan los bloque specify de verilog de forma electiva, implementando un módulo conductual, así como un scoreboard y un módulo sintetizado de forma automática con Yosys. Se realiza la temporazación basado en componentes disponibles comercialmente, con diferencias grandes entre sus tiempos de propagación.

### Instrucciones de ejecución
Para la ejecución general de la verificación, basta con **ejecutar el comando:**
Para ejecutar **la síntesis automática**:
```bash
make synth
```

Para ejecutar la verificación **sin tiempos de propagación**:
```bash
make notiming
```
Para ejecutar la verificación **con tiempos de propagación**:
```bash
make timing
```

**En la raíz del repositorio**, estos comando llaman al makefile que se encuentra en esta ubicación, ejecutan la compilación de las pruebas que requieren ser compiladas y muestra los resultados en GTKWave. 

