#!/bin/bash
notiming:
	iverilog -o counter tb_top.v
	vvp counter
	gtkwave test.vcd

timing:
	iverilog -o counter -gspecify tb_top.v
	vvp counter
	gtkwave test.vcd

synth:
	yosys script.ys
	sed -i 's/counter/counter_synth/g' contador_synth.v
	sed -i '1s/^/`timescale 	1ns \/ 1ps\n/' contador_synth.v
