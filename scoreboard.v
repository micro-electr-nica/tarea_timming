`timescale 	1ns / 1ps
module scoreboard (enable, clk, reset, mode, D, Q, rco, load);

  //Input vars definition
  input clk, enable, reset;
  input [1:0] mode;
  input [3:0] D;

  //Output vars definition
  output reg [3:0] Q;
  output reg rco, load;

  wire Ze;

  initial begin
    Q <= 0;
    rco <= 0;
  end

  always @(posedge clk) begin

    if (enable && ~reset) begin
      if (mode == 2'b00) begin
        Q <= Q+3;
        load <= 0;
        if (Q == (4'b1111) | (4'b0000)) rco <= 1;
        else rco <= 0; 
      end

      if (mode == 2'b01) begin      load = 0;
        Q <= Q-1;
        load <= 0;
        if ((Q == 4'b1111)| (4'b0000)) rco <= 1;
        else rco <= 0; 
      end

      if (mode == 2'b10) begin
        Q <= Q+1;
        load <= 0;
        if (Q == 4'b1111 | (4'b0000)) rco <= 1;
        else rco <= 0; 
      end

      if (mode == 2'b11) begin
        Q <= D;
        load <= 1;
        rco <= 0;
      end
    end

    else if (reset) begin
      Q <= 'b0;
      rco <= 'b0;
      load <= 'b0;
    end

    else if (~reset && ~enable) begin
      Q <= Ze;
      rco <= Ze;
      load <= Ze;
    end

  end

endmodule

