`include "counter_tb.v"
`timescale 	1ns / 1ps

module tb_top;

    wire clk, rco, load, enable, reset;
    wire [3:0] D, Q;
    wire [1:0] mode;

    counter_tb tb (
        .enable (enable),
        .reset (reset), 
        .mode (mode),
        .load (load),
        .D (D)
    );

endmodule