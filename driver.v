task drv_init;
  begin
    @(posedge clk) begin
      reset = 1;
      enable = 0;
    end
    @(posedge clk) begin
      reset = 0;
      enable = 1;
    end
    @(posedge clk) begin
      reset = 0;
      enable = 1;
    end
  end
endtask

task rand_counter;
  input integer iteration;
  repeat (iteration) begin  
    @(posedge clk) begin
        D[0] = $random;
        D[1] = $random;
        D[2] = $random;
        D[3] = $random;
        mode[0] = $random;
        mode[1] = $random;
    end
  end
endtask

task fixed_counter;
  input integer iteration;
  repeat (iteration) begin  
    D = $random;
    @(posedge clk) begin
        mode <= 'b00;
        @(posedge clk);
        @(posedge clk);
        @(posedge clk);
        @(posedge clk);
        mode <= 'b01;
        @(posedge clk);
        @(posedge clk);
        @(posedge clk);
        @(posedge clk);
        mode <= 'b10;
        @(posedge clk);
        @(posedge clk);
        @(posedge clk);
        @(posedge clk);
        mode <= 'b11;
        @(posedge clk);
        @(posedge clk);
        @(posedge clk);
        @(posedge clk);
    end
  end
endtask

task reset_breaker;
  begin
    @(posedge clk) begin
      reset = 'b0;
      enable = 'b0;
    end
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    reset = 'b1;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk) begin
      reset = 'b0;
      enable = 'b1;
    end
    @(posedge clk);
    @(posedge clk);
  end
endtask
