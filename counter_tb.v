`include "scoreboard.v"
`include "clock.v"
`include "contador_synth.v"
`include "contador.v"
`include "cmos_cells.v"

`timescale 	1ns / 1ps


module counter_tb(enable, reset, mode, D, load);
    
    output reg enable, reset;
    output reg [1:0] mode;
    output reg [3:0] D;
    output load;
    
    `include "driver.v"
    `include "checker.v"
    
    parameter ITERATIONS = 10;
    integer log;

    wire [3:0] sb_Q, Q, Q_synth;
    wire sb_rco, sb_load, rco, clk;
    
    initial begin
        $dumpfile("test.vcd");
        $dumpvars(0);
    
        log = $fopen("tb.log");
    
        $fdisplay(log, "time=%5d, Simulation Start\n", $time);
        $fdisplay(log, "time=%5d, Starting Reset", $time);
        drv_init();
        $fdisplay(log, "time=%5d, Reset Completed\n\n", $time);


        /*              FIXED VALUES TEST             */
        $fdisplay(log, "time=%5d, Starting FIXED Test", $time);
        fork 
            fixed_counter(ITERATIONS);
            checker(ITERATIONS);
        join
        $fdisplay(log, "time=%5d, FIXED VALUES Test Completed\n\n", $time);



        /*              RESET/ENABLE TEST             */
        $fdisplay(log, "time=%5d, Starting RESET/ENABLE Test", $time);
        fork
            reset_breaker();
            checker(ITERATIONS);
        join
        $fdisplay(log, "time=%5d, RESET/ENABLE Test Completed\n\n", $time);


        /*              RAMDOM TEST             */
        $fdisplay(log, "time=%5d, Starting RANDOM Test", $time);
        fork
            rand_counter(ITERATIONS);  
            checker(ITERATIONS);
        join
        $fdisplay(log, "time=%5d, RANDOM Test Completed\n\n", $time);


        $fdisplay(log, "time=%5d, Simulation Completed", $time);
        $fclose(log);
        #200 $finish;
    end

    clk_mod clock (
        .clock (clk)
    );

    scoreboard sb(
        .enable (enable),
        .clk (clk),
        .reset (reset),
        .mode (mode),
        .D (D),
        .Q (sb_Q),
        .rco (sb_rco),
        .load (sb_load)
    );

    counter cond(
        .clk (clk),
        .enable (enable),
        .reset (reset),
        .D (D),
        .MODO (mode),
        .Q (Q),
        .rco (rco),
        .load (load)
    ); 
    
    counter_synth estr(
    .clk (clk),
    .reset (reset),
    .enable(enable),
    .D (D),
    .MODO (mode),
    .Q (Q_synth),
    .rco (rco),
    .load (load)
); 

endmodule