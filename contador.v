`timescale 	1ns / 1ps
module counter (input clk,
                input enable,
                input reset,
                input [3:0] D,
                input [1:0] MODO,
                output reg [3:0] Q,
                output reg rco,
                output reg load
                );
    wire [3:0] Ze;
    wire z;

    always @(posedge clk) begin
        if (!reset & ~enable) begin
            Q <= Ze;
            rco <= z;
            load <= z;
        end
        else if (reset) begin
            Q <= 'b0;
            rco <= 'b0;
            load <= 'b0;
        end

        else if (!enable) begin
            Q <= D;
            rco <= 1;
            load <= 0;
        end

        else begin
            case (MODO)
                'b00: begin
                    Q <= Q+3;
                    load <= 0;
                    if (Q==4'hF| Q== 4'hE | Q== 4'hD) rco <= 1;
                    else rco <= 0;
                end

                'b01: begin
                    Q <= Q-1;
                    load <= 0;
                    if (Q==4'hF) rco <= 1;
                    else rco <= 0;
                end

                'b10: begin
                    Q <= Q+1;
                    load <= 0;
                    if (Q==4'hF) rco <= 1;
                    else rco <= 0;
                end

                'b11: begin 
                    Q <= D;
                    load <= 1;
                    rco <= 0;
                end
            endcase
        end
    end
endmodule