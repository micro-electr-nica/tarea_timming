task monitor;

reg [3:0] mon_Q, mon_D;
reg [1:0] mon_mode;
reg mon_rco, mon_load, mon_enable;

forever always (posedge clk)begin
    mon_Q <= Q;
    mon_D <= D;
    mon_mode <= mode;
    mon_rco <= rco;
    mon_load <= load;
    mon_enable <= enable;
end

endtask
