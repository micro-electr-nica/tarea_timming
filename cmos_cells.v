`timescale 	1ns / 1ps

module FF_D ( output reg Q,
              input D,
              input C);
    //TI: SN74LVC1G80-Q1 @5V; https://www.ti.com/lit/ds/symlink/sn74lvc1g80-q1.pdf?ts=1588863392876

    specify
        specparam Thi = 2.5, 
                  Tlow = 2.5,
                  Tset = 1.1,
                  Thold = 0.4;
        (D *> Q) = (Tlow, Thi);
        $setup(D, posedge clk, Tset);
        $hold(posedge C, D, Thold);
    endspecify
    always @(posedge C) begin
        Q <= D;
    end
endmodule

module NAND ( output Y,
               input A,
               input B);
    //TI: CD4011B @5V; https://www.ti.com/lit/ds/symlink/cd4011b.pdf
    specify
        specparam Thi = 125,
                  Tlow = 125;
        (A, B *> Y) = (Tlow, Thi);
    endspecify
  assign Y = ~(A & B);
endmodule

module NAND3 ( output Y,
               input A,
               input B,
               input C);
      //TI: CD4023B @5V; https://www.ti.com/lit/ds/symlink/cd4011b.pdf
    specify
      specparam Thi = 60,
                Tlow = 45;
      (A, B, C *> Y) = (Tlow, Thi);
    endspecify
  assign Y = ~(A & B & C);
endmodule

module NOR ( output Y,
              input A,
              input B);
    //TI: SN74LVC1G02 @5V, C= 30pF; https://www.ti.com/lit/ds/symlink/sn74lvc1g02.pdf?ts=1588862588071
    specify
      specparam Thi = 1,
                Tlow = 1;
      (A, B *> Y) = (Tlow, Thi);
    endspecify
  assign Y = ~(A | B);
endmodule

module NOR3 ( output Y,
              input A,
              input B,
              input C);

    //TI: CD4025B @5V; https://www.ti.com/lit/ds/symlink/cd4025b.pdf?ts=1601963197604&ref_url=https%253A%252F%252Fwww.ti.com%252Fstore%252Fti%252Fen%252Fp%252Fproduct%252F%253Fp%253DCD4025BPW
    specify
      specparam Thi = 125,
                Tlow = 125;
      (A, B, C *> Y) = (Tlow, Thi);
    endspecify
  assign Y = ~(A | B | C);
endmodule

module NOT ( output Y,
             input A);
    //TI: SN74LV1T04 @5V, 30pF; https://www.ti.com/lit/ds/symlink/sn74lv1t04.pdf?ts=1588862169682
    specify
      specparam Thi = 5.5,
                Tlow = 5.5;
      (A *> Y) = (Tlow, Thi);
    endspecify
  assign Y = ~A;
endmodule