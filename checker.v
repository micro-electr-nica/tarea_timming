task checker;

input integer iteration;

repeat (iteration) @ (posedge clk) begin
    if ({sb.Q, sb.rco, sb.load} == {Q, rco, load}) begin
      $fdisplay(log, "PASSED counter on mode %b.", mode);
    end

    else begin
      if ({sb.Q, sb.rco, sb.load} == {Q, rco, load}) begin
        $fdisplay(log, "Time=%.0f Error in dut:\n\tQ=%b, rco=%b, load=%b\n\tscoreboard:\n\tQ=%b, rco=%b, load=%b\n", $time, Q, rco, load, sb_Q, sb_rco, sb_load);
      end
    end
  end
endtask
